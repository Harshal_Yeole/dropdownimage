import './App.css';
import React from 'react'; 
import Display from './display';
import Img1 from './vindiesel.jpg';
import Img2 from './dwaynejohnson.jpg';
import Img3 from './salmankhan.jpg';
import Img4 from './rajpalyadav.jpg';
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: ""};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    alert('Your favorite flavor is: ' + this.state.value);
    event.preventDefault();
  }

  render() {
    return (
      <div className="App">
      <form onSubmit={this.handleSubmit}>
        <label>
          <h1>Pick your favorite Actor:</h1>
          <select value={this.state.value} onChange={this.handleChange}>
          <option>Select one</option>
            <option value={Img1}>Vin Diesel</option>
            <option value={Img2}>Dwayne Johnson</option>
            <option value={Img3}>Salman Khan</option>
            <option value={Img4}>Rajpal Yadav</option>
          </select>
        </label>
        <Display img={this.state.value}/>
      </form>
      </div>
    );
  }
}

export default App;
